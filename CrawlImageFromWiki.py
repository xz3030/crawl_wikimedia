# -*- coding:utf-8 -*-
from BeautifulSoup import BeautifulSoup
import urllib,urllib2
import os
import re



def readMainPage(page, mainDir):
    myheaders =  {'Referer':'http://commons.wikimedia.org',  
                    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1',  
                }

    req = urllib2.Request(page, headers = myheaders)
    f = urllib2.urlopen(req, timeout=20)  
    
    content = f.read()
    soup = BeautifulSoup(content)
    
    # find sub category div
    subcatSoup = soup.find('div',{'id':'mw-subcategories'})
    cats = subcatSoup.findAll('a',{'class':re.compile('CategoryTreeLabel .*')})
    
    # the top 5 categories is broad ones
    style_list_fname = mainDir+'styles.txt'
    style_list=[]
    fd=open(style_list_fname,'wt')
    for i in range(5,len(cats)):
        print i-4
        soup = cats[i]
        style_name = soup.text
        style_url = soup['href']
        style_list.append([style_name, style_url])
        subcat_dir = main_dir+style_name
        if not os.path.isdir(subcat_dir):
            os.mkdir(subcat_dir)
            fd2=open(main_dir+style_name+'/url.txt','wt')
            fd2.write(style_url)
            fd2.close()
        fd.write(style_name+'\n')
        
    fd.close()
    

def readSubCatAll(root_page, main_dir):
    styles = os.listdir(main_dir)
    print len(styles)
    styles = [s for s in styles if os.path.isdir(main_dir+s)]
    styles.sort()
    
    for s in styles[137:138]:
        fd=open(main_dir+s+'/url.txt')
        l=fd.readline()
        fd.close()
        readSubCat(root_page, root_page+l, main_dir+s, 0)
        
        

def readSubCat(root_page, url, tmpdir, img_count):
    print url
    record_file = main_dir+'record.txt'
    fd=open(record_file,'r')
    records = fd.readlines()
    fd.close()
    records = [r.replace('\n','') for r in records]
    #print 'Records:', records
    
    myheaders =  {'Referer':'http://commons.wikimedia.org',  
                  'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1',  
                }

    req = urllib2.Request(url, headers = myheaders)
    f = urllib2.urlopen(req, timeout=20)  
    
    content = f.read()
    soup = BeautifulSoup(content)
    
    # images
    imageBoxes = soup.findAll('li',{'class':'gallerybox'})
    for box in imageBoxes:
        try:
            im = box.find('a',{'class':'image'})
            imurl = root_page+im['href']
            
            #download image
            req = urllib2.Request(imurl, headers = myheaders)
            f = urllib2.urlopen(req, timeout=20)
            imsoup=BeautifulSoup(f)
            x=imsoup.find('div',{'class':'fullImageLink'})
            y=x.find('img')
            srcurl='http:'+y['src']
            print img_count, tmpdir+'/'+srcurl.split('/')[-1]
            localFileName = tmpdir+'/'+srcurl.split('/')[-1]
            if not os.path.isfile(localFileName):
                urllib.urlretrieve(srcurl, localFileName)
                img_count = img_count+1
            else:
                print 'Already exists'
        except:
            print 'Image failed'
        if img_count>5000:
            img_count = img_count+1
            exit(0)
        
        
    # categories
    cats = soup.findAll('a',{'class':re.compile('CategoryTreeLabel .*')})
    for subc in cats:
        style_name = subc.text
        style_name = style_name.replace('/','_')
        if style_name.find('elements')!=-1:
            continue
        style_url = subc['href']
        
        if records.count(style_name)==0:
            fd=open(record_file,'a')
            fd.write(style_name+'\n')
            fd.close()
        
            subcat_dir = tmpdir+'/'+style_name
            try:
                if not os.path.isdir(subcat_dir):
                    os.mkdir(subcat_dir)
                fd2=open(subcat_dir+'/url.txt','wt')
                fd2.write(style_url)
                fd2.close()
                img_count = readSubCat(root_page, root_page+style_url, subcat_dir, img_count)
            except:
                continue
        else:
            print 'Artictecture '+style_name+' already exists'
            for ii in range(5):
                print '******************************' 
    return img_count


if __name__=='__main__':
    root_page='http://commons.wikimedia.org'
    main_page='http://commons.wikimedia.org/wiki/Category:Architecture_by_style'
    main_dir = '/home/zhenxu/workspace/ArchitectureStyle/'
    
    print 'Start Analyzing main page'
    if not os.path.isfile(main_dir+'styles.txt'):
        readMainPage(main_page, main_dir)

    print 'Start Reading SubCategories'
    readSubCatAll(root_page, main_dir)