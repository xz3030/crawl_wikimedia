# -*- coding:utf-8 -*-
from BeautifulSoup import BeautifulSoup
import urllib,urllib2
import os
import re



def readMainPage(page, mainDir):
    myheaders =  {'Referer':'http://en.wikimedia.org',  
                    'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1',  
                }

    req = urllib2.Request(page, headers = myheaders)
    f = urllib2.urlopen(req, timeout=20)  
    
    content = f.read()
    soup = BeautifulSoup(content)
    
    # find sub category div
    subcatSoup = soup.find('div',{'id':'mw-subcategories'})
    cats = subcatSoup.findAll('a',{'class':'CategoryTreeLabel  CategoryTreeLabelNs14 CategoryTreeLabelCategory'})
    
    # the top 5 categories is broad ones
    style_list_fname = mainDir+'countries.txt'
    style_list=[]
    fd=open(style_list_fname,'wt')
    for i in range(0,len(cats)):
        print i
        soup = cats[i]
        style_name = soup.text
        style_url = soup['href']
        style_list.append([style_name, style_url])
        subcat_dir = main_dir+style_name
        if not os.path.isdir(subcat_dir):
            os.mkdir(subcat_dir)
            fd2=open(main_dir+style_name+'/url.txt','wt')
            fd2.write(style_url)
            fd2.close()
        fd.write(style_name+'\n')
        
    fd.close()
    

def readSubCatAll(root_page, main_dir):
    styles = os.listdir(main_dir)
    print len(styles)
    styles = [s for s in styles if os.path.isdir(main_dir+s)]
    styles.sort()
    count_all = 0
    
    for ss in range(len(styles)):
        s = styles[ss]
        fd=open(main_dir+s+'/url.txt')
        l=fd.readline()
        fd.close()
        try:
            actor_count = readSubCat(root_page, root_page+l, main_dir+s, 0)
            count_all = count_all+actor_count
        except:
            pass
        print count_all
        
    
    print count_all
        
        

def readSubCat(root_page, url, tmpdir, img_count):
    print url
    print tmpdir
    #print 'Records:', records
    records = []
    
    myheaders =  {'Referer':'http://en.wikimedia.org',  
                  'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1',  
                }

    req = urllib2.Request(url, headers = myheaders)
    f = urllib2.urlopen(req, timeout=20)  
    
    content = f.read()
    soup = BeautifulSoup(content)
    
    actor_count = 0
    
    isContinue = 1
    
    while isContinue:
        pages_soup = soup.find('div',{'id':'mw-pages'})
        # names
        nameBoxes = pages_soup.findAll('li')
        for box in nameBoxes:
            try:
                name = box.text
                print name
                if records.count(name)==0:
                    records.append(name)
                    actor_count = actor_count+1
            except:
                pass
                    
                    
        isContinue = 0
        prev_next = pages_soup.findAll('a',{'title':re.compile('Category:*')})
        
        for p in range(len(prev_next)):
            pp=prev_next[p]
            if pp.text=='next 200':
                isContinue = 1
                nextpage = pp['href']
                
                url1 = root_page+nextpage
                req = urllib2.Request(url1, headers = myheaders)
                f = urllib2.urlopen(req, timeout=20)  
                
                content = f.read()
                soup = BeautifulSoup(content)
                print 'next_page'
                break
                
    print records
    record_file = tmpdir+'/names.txt'
    fd=open(record_file,'wt')
    fd.writelines('\n'.join(records))
    fd.close()
    
    return actor_count
        
    '''
    # categories
    cats = soup.findAll('a',{'class':re.compile('CategoryTreeLabel .*')})
    for subc in cats:
        style_name = subc.text
        style_name = style_name.replace('/','_')
        if style_name.find('elements')!=-1:
            continue
        style_url = subc['href']
        
        if records.count(style_name)==0:
            fd=open(record_file,'a')
            fd.write(style_name+'\n')
            fd.close()
        
            subcat_dir = tmpdir+'/'+style_name
            try:
                if not os.path.isdir(subcat_dir):
                    os.mkdir(subcat_dir)
                fd2=open(subcat_dir+'/url.txt','wt')
                fd2.write(style_url)
                fd2.close()
                img_count = readSubCat(root_page, root_page+style_url, subcat_dir, img_count)
            except:
                continue
        else:
            print 'Artictecture '+style_name+' already exists'
            for ii in range(5):
                print '******************************' 
    return img_count
    '''

if __name__=='__main__':
    root_page='http://en.wikipedia.org'
    main_page='http://en.wikipedia.org/wiki/Category:Male_film_actors_by_nationality'
    main_dir = '/home/zhenxu/workspace/ActorName/male/'
    
    #print 'Start Analyzing main page'
    #if not os.path.isfile(main_dir+'countries.txt'):
    #    readMainPage(main_page, main_dir)

    print 'Start Reading SubCategories'
    readSubCatAll(root_page, main_dir)