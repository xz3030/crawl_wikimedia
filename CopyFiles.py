'''
copy images to a new place so that the filtering process can be done more easily
'''

import os, os.path


def cpims(path, destdir):
    count = 0
    for root,dirs,files in os.walk(path):
        for filespath in files:
            if filespath.find("txt")!=-1:
                continue
            dstim = destdir+'/'+str(count)+'_'+filespath
            srcim = os.path.join(root,filespath)
            cmd = "cp \"%s\" \"%s\""%(srcim,dstim)
            os.system(cmd)
            count += 1
            if count%1000==0:
                print count
    print 'End ~', count

def main(cid):
    main_dir = '/home/zhenxu/workspace/ArchitectureStyle/'
    dest_dir = '/home/zhenxu/workspace/ArchitectureStyle_filtered/'
    
    cls = os.listdir(main_dir)
    cls.sort()
    cl = main_dir+cls[cid]
    print cl
    
    if os.path.isdir(dest_dir+cls[cid]):
        return
    os.mkdir(dest_dir+cls[cid])
    cpims(cl, dest_dir+cls[cid])
    print cls
    print len(cls)
    
    
    
    
main(139)